$().ready(function () {
    $("#cmsForm").validate({
        rules: {
            title: "required",
            alias: "required"
        },
        messages: {
            title: "Please enter Title"
        }
    });
})


var chars = new Array(" ", "_", "@", "'", "&", "\/", ",", "'", '"');
var replaceWith = new Array('-', '-', '-', "-", "x", "x", "", "", "");
$('#title').keyup(function () {
    var alias = replaceChars(chars, replaceWith, $(this).val());
    $('#alias').val(alias.toLowerCase());
});

function replaceChars(fromAr, replaceWithAr, str)
{
    var char = fromAr;
    var charReplace = replaceWithAr;
    for (var i = 0, len = char.length; i < len; i++) {
        var reg_exp = new RegExp(char[i], 'g');
        str = str.replace(reg_exp, charReplace[i]);
    }
    return str;
}
