<?php

/**
 * Created by PhpStorm.
 * User: Gaurav Ranjan<ranjan.gaurav@orangemantra.in>
 * Date: 05/10/2016
 * Time: 11:28 AM
 */
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';
require APPPATH . '/libraries/aws.phar';
class User extends REST_Controller {
	function __construct() {
		// Construct the parent class
		parent::__construct ();
		$this->load->model ( 'user_model', 'user' );
		$this->load->helper ( 'string' );
		
	}
	
	/**
	 *
	 * @method : Method to send an oTP
	 * @method description: Send OTP Message
	 * @param
	 *        	: user_mobile
	 *        	@data: Send oTp Data
	 */
	public function otp_post() {
		$mobile = $this->post ( 'user_mobile' );
		$country_code = $this->post ( 'country_code' );
		if ($mobile != '' && $mobile != null) {
			$randcode = random_string ( 'numeric', 16 );
			$random_code = substr ( $randcode, 0, 4 );
			$sns = Aws\Sns\SnsClient::factory ( array (
					'version' => '2010-03-31',
					'region' => 'ap-southeast-1',
					'credentials' => array (
							'key' => 'AKIAJAC3UZE2HG244DNQ',
							'secret' => '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' 
					) 
			) );
			$result = $sns->listSubscriptionsByTopic ( array (
					'TopicArn' => 'arn:aws:sns:ap-southeast-1:581428970236:woytrip-stage-reg' 
			) );
			foreach ( $result as $key => $value ) {
				if ($key == 'Subscriptions' && count ( $value )) {
					foreach ( $value as $k => $v ) {
						$result = $sns->unsubscribe ( array (
								// SubscriptionArn is required
								'SubscriptionArn' => $v ['SubscriptionArn'] 
						) );
					}
				}
			}
			$sns->subscribe ( array (
					// TopicArn is required
					'TopicArn' => 'arn:aws:sns:ap-southeast-1:581428970236:woytrip-stage-reg',
					// Protocol is required
					'Protocol' => 'sms',
					//'SMSType' => 'Transactional',
					'Endpoint' => '+91' . $mobile 
			) );
			$res = $sns->publish ( array (
					'TopicArn' => 'arn:aws:sns:ap-southeast-1:581428970236:woytrip-stage-reg',
					'Message' => "Your OTP is $random_code" 
			) );
			
			$this->set_response ( [ 
					'status' => true,
					'response_code' => '1',
					'otp' => $random_code,
					'message' => 'Success' 
			], REST_Controller::HTTP_OK );
		} else {
			$this->set_response ( [ 
					'status' => FALSE,
					'response_code' => '0',
					'message' => 'Please enter mobile number.' 
			], REST_Controller::HTTP_OK );
		}
	}
	
	/**
	 *
	 * @method : Methods to add an user
	 * @method description: Add User
	 * @param        	
	 *
	 * @author : Gaurav Ranjan
	 *         : details
	 *         @data: add new user to wo_users table
	 */
	public function create_post() {
		error_reporting ( 0 );
		try {
			foreach ( (($this->post ())) as $key => $value ) {
				log_message ( 'info', 'data=' . $key . ' =>' . $value );
			}
			$mobile = $this->input->post ( 'user_mobile' );
			
			/*
			 * To replace Country code and Zero at first occurence of mobile Number
			 * Author : Gaurav Ranjan
			 * Date : 15-11-2016
			 *
			 *
			 *
			 */
			
			if (0 === strpos ( $mobile, '0' )) {
				$mobile = $this->str_replace_first ( '0', '', $mobile );
			}
			
			if (0 === strpos ( $mobile, '91' )) {
				$mobile = $this->str_replace_first ( '91', '', $mobile );
			}
			
			if (0 === strpos ( $mobile, '+91' )) {
				$mobile = $this->str_replace_first ( '+91', '', $mobile );
			}
			
			$checkUser = $this->user->checkUser ( $mobile ); // print_r($checkUser); die();
			if ($checkUser) {
				$status = $this->user->updateStatus ( $mobile ); // print_r($status); die();
				$friends = ($this->user->getUserFriendsList ( $checkUser->user_id )) ? $this->user->getUserFriendsList ( $checkUser->user_id ) : array ();
				$tripstatus = $this->user->GetTripStatus ( $checkUser->user_id );
				if ($tripstatus != 0) {
					$isTripStatus = true;
				} else {
					$isTripStatus = false;
				}
				$this->user->userDeviceInformations ( $checkUser->user_id );
				$message = array (
						'status' => true,
						'response_code' => '1',
						'isTripActive' => $isTripStatus,
						'message' => 'Already Registered! Please Continue',
						'data' => $checkUser,
						'friends' => $friends,
						'isTripData' => $tripstatus 
				);
			} else {
				$create = $this->user->create ( $mobile );
				
				$message = $create;
			}
			$this->set_response ( $message, REST_Controller::HTTP_OK );
		} catch ( Exception $ex ) {
			$message = array (
					'status' => false,
					'response_code' => '0',
					'message' => $ex->getMessage () 
			);
			$this->set_response ( $message, REST_Controller::HTTP_OK );
		}
	}
	function str_replace_first($from, $to, $subject) {
		$from = '/' . preg_quote ( $from, '/' ) . '/';
		
		return preg_replace ( $from, $to, $subject, 1 );
	}
	
	/**
	 *
	 * @method : Update an user
	 * @method description: Updateing a User
	 * @param
	 *        	: user_id
	 *        	@data: UPdate user to wo_users table
	 */
	public function update_post() {
		try {
			foreach ( (($this->post ())) as $key => $value ) {
				log_message ( 'info', 'data=' . $key . ' =>' . $value );
			}
			$userId = $this->post ( 'user_id' );
			if ($userId == '') {
				$message = array (
						'status' => false,
						'response_code' => '0',
						'message' => 'Please send user id' 
				);
				$this->set_response ( $message, REST_Controller::HTTP_OK );
			} else {
				$update = $this->user->update ( $userId );
				$this->set_response ( $update, REST_Controller::HTTP_OK );
			}
		} catch ( Exception $ex ) {
			$message = array (
					'status' => false,
					'response_code' => '0',
					'message' => $ex->getMessage () 
			);
			$this->set_response ( $message, REST_Controller::HTTP_OK );
		}
	}
	
	/*
	 * To refresh the friend list of a user
	 * @Params: user_id
	 * @Table: wo_users
	 */
	public function friends_post() {
		try {
			// echo 234; die();
			$userID = $this->post ( 'user_id' );
			$friends = $this->user->getUserFriendsList ( $userID );
			if (! empty ( $friends )) {
				$message = array (
						'status' => true,
						'response_code' => '1',
						'message' => 'Success',
						'friends' => $friends 
				);
			} else {
				$message = array (
						'status' => false,
						'response_code' => '0',
						'message' => 'No friends available' 
				);
			}
			$this->set_response ( $message, REST_Controller::HTTP_OK );
		} catch ( Exception $ex ) {
			$message = array (
					'status' => false,
					'response_code' => '0',
					'message' => $ex->getMessage () 
			);
			$this->set_response ( $message, REST_Controller::HTTP_OK );
		}
	}
	
	/*
	 * To get the Profile Information Based on user_id
	 * @Params: user_id
	 * @Table: wo_users
	 */
	public function profile_post() {
		try {
			$user_id = ( int ) $this->post ( 'user_id' );
			$info = $this->user->getUserInfoById ( $user_id );
			if (! empty ( $info )) {
				$message = array (
						'status' => true,
						'response_code' => '1',
						'message' => 'Success',
						'data' => $info 
				);
			} else {
				$message = array (
						'status' => true,
						'response_code' => '1',
						'message' => 'No Information Available.Please send user Id' 
				);
			}
		} catch ( Exception $ex ) {
			$message = array (
					'status' => false,
					'response_code' => '0',
					'message' => $ex->getMessage () 
			);
		}
		$this->set_response ( $message, REST_Controller::HTTP_OK );
	}
}
