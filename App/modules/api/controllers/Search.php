<?php

/**
 * Created by PhpStorm.
 * User: Gaurav Ranjan<ranjan.gaurav@orangemantra.in>
 * Date: 05/10/2016
 * Time: 11:28 AM
 */
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';
// require APPPATH . '/libraries/aws.phar';
class Search extends REST_Controller {
	function __construct() {
		// Construct the parent class
		parent::__construct ();
		$this->load->model ( 'user_model', 'user' );
		$this->load->model ( 'Search_model', 'search' );
		$this->load->helper ( 'string' );
	}
	
	/*
	 * Author : Gaurav Ranjan
	 *
	 * Date : 06/01/2017
	 *
	 * Description : API for Search Trips or Users or groups .
	 *
	 * Hint : Remove userid paramenter if you want to search globally.
	 *
	 */
	public function Search_post() {
		try {
			$text = $this->input->post ( 'searchtext' );
			$userid = $this->input->post ( 'userid' );
			if ($text) {
				
				$friends = $this->search->GetAllFriends ( $userid );
				$group = $this->search->GetAllgroupID ( $userid );
				$friends = array_column ( $friends, 'friend_id' );
				$group = array_column ( $group, 'group_id' ); // print_r($group); die();
				
				$users = $this->search->getUsersData ( $text, $userid, $friends );
				$trips = $this->search->getAlltrips ( $text, $userid, $friends );
				$groups = $this->search->getAllgroups ( $text, $userid, $group );
				
				// print_r($users); die();
				$message = array (
						'status' => true,
						'response_code' => '1',
						'message' => 'Success',
						'data' => array (
								array (
										'users' => $users,
										'trips' => $trips,
										'groups' => $groups 
								) 
						) 
				);
			} 

			else {
				
				$message = array (
						'status' => false,
						'response_code' => '0',
						'message' => 'Please enter your text' 
				);
			}
		} 

		catch ( Exception $ex ) {
			$message = array (
					'status' => false,
					'response_code' => '0',
					'message' => $ex->getMessage () 
			);
		}
		
		$this->set_response ( $message, REST_Controller::HTTP_OK );
	}
}
