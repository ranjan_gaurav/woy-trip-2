<?php

/**
 * Created by PhpStorm.
 * User: Gaurav Ranjan <ranjan.gaurav@orangemantra.in>
 * Date: 08/10/2016
 * Time: 11:28 AM
 */
class Trip_model extends CI_Model {
	var $activity_table = "wo_activities";
	var $trip_table = "wo_trips";
	var $comment_table = "wo_activity_comments";
	var $like_table = "wo_activity_likes";
	var $media_table = "media";
	var $user_table = "wo_users";
	var $friends_table = "wo_friends";
	var $place_table = "wo_places";
	var $trip_mates = "wo_trip_mates";
	function __construct() {
		parent::__construct ();
	}
	
	/*
	 * Query to enlist all trips
	 * @params: user_id , type ( user / friends)
	 * @Table: wo_trips
	 * @Author: Gaurav Ranjan
	 */
	public function getTrips($user_id, $start_from) {
		$this->db->limit ( 10, $start_from );
		$this->db->distinct ();
		$this->db->select ( 'tr.trip_id,tr.trip_status,tr.place_id,tr.destination as place_name,tr.title as trip_name , tr.start_date,tr.end_date, EXTRACT(MONTH FROM tr.creation_date) as month , EXTRACT(YEAR FROM tr.creation_date) as Year, IF(us.user_pic = "","null",CONCAT("' . base_url ( 'uploads/users/profile' ) . '/",us.user_pic)) as user_pic,us.name,us.user_id,us.user_mobile,IF(pl.place_banner = "","null",CONCAT("' . base_url ( 'uploads/media' ) . '/",pl.place_banner)) as place_banner' );
		$this->db->where ( 'tr.user_id', $user_id );
		$this->db->order_by ( 'tr.creation_date', 'DESC' );
		$this->db->from ( $this->trip_table . ' tr' );
		$this->db->join ( $this->user_table . ' us', 'us.user_id=tr.user_id' );
		$this->db->join ( $this->place_table . ' pl', 'pl.place_id=tr.place_id', 'left' );
		$this->db->join ( $this->activity_table . ' act', 'act.resource_id=tr.trip_id', 'left' );
		$this->db->join ( $this->media_table . ' m', 'm.media_id=act.media_id', 'left' );
		$this->db->join ( $this->trip_mates . ' tm', 'tm.trip_id=tr.trip_id', 'left' );
		$res = $this->db->get ();
		// echo $this->db->last_query ();
		// die ();
		
		$arr = array ();
		foreach ( $res->result_array () as $row ) {
			$med = $this->GetMedia ( $row ['trip_id'] );
			$row ['place_banner'] = $med->media_src;
			if ($row ['trip_name'] == '') { // Comments :
			                                // Trip Name should be optional and if trip name is not given then Default Trip name as � Trip to [Destination] [ Month] [Year ]� should be generated.
				$monthNum = $row ['month'];
				$dateObj = DateTime::createFromFormat ( '!m', $monthNum );
				$monthName = $dateObj->format ( 'F' );
				$row ['trip_name'] = "Trip to " . $row ['place_name'] . ' ' . $monthName . ' ' . $row ['Year'];
			}
			unset ( $row ['month'] );
			unset ( $row ['Year'] );
			array_push ( $arr, $row );
		}
		
		// print_r($arr); die();
		return $arr;
	}
	public function GetMedia($tripID) {
		error_reporting ( 0 );
		$this->db->limit ( 1 );
		$this->db->order_by ( 'wo_activities.creation_date' );
		$this->db->where ( 'wo_activities.resource_id', $tripID );
		$this->db->select ( 'CONCAT("' . base_url ( 'uploads/media' ) . '/",media_gallery.media_src) as media_src' );
		$this->db->from ( $this->activity_table );
		$this->db->join ( 'media_gallery', 'wo_activities.activity_id = media_gallery.activity_id and wo_activities.activity_type ="wall"', 'inner' );
		$res = $this->db->get (); // echo $this->db->last_query(); //die();
		return $res->row ();
	}
	public function getFriendsTrips($user_id, $friends, $start_from) {
		
		// echo 123; die();
		$this->db->limit ( 10, $start_from );
		$this->db->select ( 'tr.trip_id,tr.trip_status,tr.place_id,tr.destination as place_name,tr.title as trip_name,tr.start_date,tr.end_date,EXTRACT(MONTH FROM tr.creation_date) as month , EXTRACT(YEAR FROM tr.creation_date) as Year,IF(us.user_pic = "","null",CONCAT("' . base_url ( 'uploads/users/profile' ) . '/",us.user_pic)) as user_pic,us.name,us.user_id,us.user_mobile,IF(pl.place_banner = "","null",CONCAT("' . base_url ( 'uploads/media' ) . '/",pl.place_banner)) as place_banner' );
		$friends = array_column ( $friends, 'friend_id' );
		$this->db->where_in ( 'tr.user_id', $friends );
		// $this->db->where ( 'tr.user_id', $user_id );
		$this->db->order_by ( 'tr.creation_date', 'DESC' );
		$this->db->from ( $this->trip_table . ' tr' );
		// $this->db->join ( $this->activity_table . ' act', 'act.resource_id=tr.trip_id' );
		$this->db->join ( $this->user_table . ' us', 'us.user_id=tr.user_id' );
		$this->db->join ( $this->place_table . ' pl', 'pl.place_id=tr.place_id', 'left' );
		$res = $this->db->get (); // echo $this->db->last_query(); die();
		                          // return $res->result_array ();
		$arr = array ();
		foreach ( $res->result_array () as $row ) {
			$med = $this->GetMedia ( $row ['trip_id'] );
			$row ['place_banner'] = $med->media_src;
			if ($row ['trip_name'] == '') { // Comments :
			                                // Trip Name should be optional and if trip name is not given then Default Trip name as � Trip to [Destination] [ Month] [Year ]� should be generated.
				$monthNum = $row ['month'];
				$dateObj = DateTime::createFromFormat ( '!m', $monthNum );
				$monthName = $dateObj->format ( 'F' );
				$row ['trip_name'] = "Trip to " . $row ['place_name'] . ' ' . $monthName . ' ' . $row ['Year'];
			}
			
			$PostCount = $this->GetPostCount ( $row ['trip_id'], $user_id ); // print_r($PostCount); die();
			if ($PostCount > 0) {
				// echo 123;
				$row ['post_count'] = $PostCount;
			} 

			else {
				// echo 90;
				$row ['post_count'] = 0;
			}
			unset ( $row ['month'] );
			unset ( $row ['Year'] );
			array_push ( $arr, $row );
		}
		
		// print_r($arr); die();
		return $arr;
	}
	public function GetPostCount($trip_id, $userid) {
		/* // $this->db->distinct();
		$this->db->where ( 'wo_trips.trip_id', $trip_id );
		$this->db->where ( 'wo_activities_views.user_id', $userid );
		$this->db->where ( 'wo_activities_views.status', 0 );
		$this->db->select ( 'count(wo_activities_views.id) as PostCount' );
		$this->db->from ( $this->trip_table );
		$this->db->join ( 'wo_activities', 'wo_activities.resource_id = wo_trips.trip_id and wo_activities.activity_type = "wall"' );
		$this->db->join ('wo_group_members','wo_activities.group_id = wo_group_members.group_id');
		$this->db->join ( 'wo_activities_views', 'wo_activities.activity_id = wo_activities_views.activity_id' );
		 */
		// $this->db->group_by('wo_trips.trip_id');
		//$result = $this->db->get ();  echo $this->db->last_query(); die();
		
		
		
		$query = "SELECT count(wo_activities_views.id) as PostCount
		FROM `wo_trips`
		JOIN `wo_activities` ON `wo_activities`.`resource_id` = `wo_trips`.`trip_id` and `wo_activities`.`activity_type` = 'wall'
		JOIN  wo_group_members on FIND_IN_SET (wo_group_members.group_id , wo_activities.group_id) > 0
		JOIN `wo_activities_views` ON `wo_activities`.`activity_id` = `wo_activities_views`.`activity_id`
		WHERE `wo_trips`.`trip_id` = $trip_id
		AND `wo_activities_views`.`user_id` = $userid
		AND `wo_group_members`.`user_id` = $userid
		AND `wo_activities_views`.`status` =0";
		
		
		$result = $this->db->query($query);  //print_r($row); die();
		return $result->row ()->PostCount;
		
		// return $count;
	}
	public function GetPostCount2($trip_id, $user_id) {
		$this->db->distinct ();
		$this->db->where ( 'wo_trips.trip_id', $trip_id );
		$this->db->where ( 'wo_activities.user_id', $userid );
		$this->db->select ( 'count(wo_activities.activity_id) as PostCount' );
		$this->db->from ( $this->trip_table );
		$this->db->join ( 'wo_activities', 'wo_activities.resource_id = wo_trips.trip_id and wo_activities.activity_type = "wall"' );
		// $this->db->join('wo_activities_views', 'wo_activities.activity_id = wo_activities_views.activity_id');
		$this->db->group_by ( 'wo_trips.trip_id' );
		$result = $this->db->get (); // echo $this->db->last_query(); die();
		$count = $result->row ()->PostCount;
		
		return $count;
	}
	
	/*
	 * Author : Gaurav Ranjan
	 * Date : 14-11-2016
	 * Description: Query to enlist all of your tripmates.
	 *
	 */
	public function GetYourTripMembers($TripID) {
		$this->db->where ( 'trip_id', $TripID );
		$this->db->where ( 'wo_users.is_active', '1' );
		$this->db->select ( 'wo_users.*,IF(wo_users.user_pic = "","null",CONCAT("' . base_url ( 'uploads/users/profile' ) . '/",wo_users.user_pic)) as user_pic' );
		$this->db->from ( $this->trip_mates );
		$this->db->join ( $this->user_table, 'wo_users.user_id=wo_trip_mates.user_id' );
		$result = $this->db->get ();
		return $result->result_array ();
	}
	public function deleteTrip($TripID) {
		$this->db->where ( 'trip_id', $TripID );
		// $this->db->where ( 'trip_status', 'completed' );
		$res = $this->db->get ( 'wo_trips' );
		$result = $res->row ();
		if ($result->trip_status == 'completed') {
			$this->db->where ( 'trip_id', $TripID );
			$delete = $this->db->delete ( 'wo_trips' );
			if ($delete) {
				$this->db->where ( 'resource_id', $TripID );
				$actDelete = $this->db->delete ( 'wo_activities' );
				if ($actDelete) {
					// echo 455466767676;
					return 1;
				} 

				else {
					
					return 0;
				}
			}
		} 

		else {
			return 0;
		}
	}
}

?>