<?php

/**
 * Created by PhpStorm.
 * User: Gaurav Ranjan <ranjan.gaurav@orangemantra.in>
 * Date: 05/10/2016
 * Time: 11:28 AM
 */
class User_model extends CI_Model {
	var $user_table = "wo_users";
	var $device_table = "wo_user_devices";
	var $friends_table = "wo_friends";
	function __construct() {
		parent::__construct ();
	}
	function str_replace_first($from, $to, $subject) {
		$from = '/' . preg_quote ( $from, '/' ) . '/';
		
		return preg_replace ( $from, $to, $subject, 1 );
	}
	/*
	 * Check User Is Already Registered or Not
	 */
	public function checkUser($mobile) {
		$countrycode = $this->input->post ( 'country_code' );
		
		$this->db->where ( 'country_code', $countrycode );
		$this->db->where ( 'user_mobile', $mobile );
		$this->db->from ( $this->user_table );
		$res = $this->db->get (); // echo $this->db->last_query(); die();
		if ($res->num_rows () > 0) {
			
			if ($res->row ()->user_pic != '') {
				
				$res->row ()->user_pic = base_url ( 'uploads/users/profile' ) . '/' . $res->row ()->user_pic;
			}
			return $res->row ();
		} else {
			return false;
		}
	}
	public function GetTripStatus($userid) {
		$this->db->where ( 'user_id', $userid );
		$this->db->where ( 'trip_status', 'ongoing' );
		$this->db->from ( 'wo_trips' );
		$res = $this->db->get (); // echo $this->db->last_query(); die();
		$result = $res->row (); // print_r($result); die();
		if ($res->num_rows () > 0) {
			
			return $res->row ();
		} else {
			return false;
		}
	}
	
	/*
	 * Create New User
	 * Table: wo_users
	 */
	public function create($mobile) {
		try {
			$this->load->library ( 'form_validation' );
			$config = array (
					array (
							'field' => 'user_mobile',
							'label' => 'Mobile',
							'rules' => 'trim|required' 
					) 
			);
			$this->form_validation->set_rules ( $config );
			if ($this->form_validation->run () == false) {
				$errors_array = '';
				foreach ( $config as $row ) {
					$field = $row ['field'];
					$error = strip_tags ( form_error ( $field ) );
					if ($error)
						$errors_array .= $error . ', ';
				}
				$message = array (
						'status' => false,
						'response_code' => '0',
						'message' => rtrim ( $errors_array, ', ' ) 
				);
			} else {
				$data = array (
						'name' => ($this->input->post ( 'name' )) ? $this->input->post ( 'name' ) : '',
						'user_mobile' => $mobile ? $mobile : '',
						'user_address' => ($this->input->post ( 'user_address' )) ? $this->input->post ( 'user_address' ) : '',
						'country_code' => ($this->input->post ( 'country_code' )) ? $this->input->post ( 'country_code' ) : '',
						'user_profile_status' => ($this->input->post ( 'user_profile_status' )) ? $this->input->post ( 'user_profile_status' ) : '',
						'is_active' => '1' 
				);
				$file = $this->uploadProfilePic ();
				if ($file ['file_name'] != '') {
					$data ['user_pic'] = $file ['file_name'];
				}
				$this->db->set ( 'creation_date', 'NOW()', false );
				$this->db->set ( 'modification_date', 'NOW()', false );
				$insert = $this->db->insert ( $this->user_table, $data );
				$user_id = $this->db->insert_id ();
				if ($insert) {
					$this->userDeviceInformations ( $user_id );
					$friends = ($this->getUserFriendsList ( $user_id )) ? $this->getUserFriendsList ( $user_id ) : array ();
					$res = $this->getUserInfoById ( $user_id );
					$message = array (
							'status' => true,
							'response_code' => '1',
							'message' => "User Added Successfully",
							'data' => $res,
							'friends' => $friends 
					);
				} else {
					$message = array (
							'status' => false,
							'response_code' => '0',
							'message' => "Database error occured" 
					);
				}
			}
		} catch ( Exception $ex ) {
			$message = array (
					'status' => false,
					'response_code' => '0',
					'message' => $ex->getMessage () 
			);
		}
		return $message;
	}
	
	/*
	 * Insert Device Informations
	 */
	public function userDeviceInformations($userid) {
		$deviceInfo = array (
				'user_id' => $userid,
				'device_id' => $this->input->post ( 'device_id' ),
				'reg_id' => $this->input->post ( 'reg_id' ),
				'fcm_id' => $this->input->post ( 'fcm_id' ),
				'device_type' => $this->input->post ( 'device_type' ) 
		);
		$checkDevice = $this->CheckUserDevice ( $deviceInfo );
		if ($checkDevice > 0) {
			$this->updateDeviceId ( $deviceInfo );
		} else {
			$this->InsertDeviceInformation ( $deviceInfo );
		}
	}
	
	/*
	 * To Check Device already is in table or not
	 * Table: wo_user_devices
	 */
	function CheckUserDevice($info) {
		$this->db->where ( array (
				'device_id' => $info ['device_id'],
				'device_type' => $info ['device_type'] 
		) );
		$res = $this->db->get ( $this->device_table );
		return $res->num_rows ();
	}
	
	/*
	 * Update the Reg_id and user for Device
	 * Table: wo_user_devices
	 */
	public function updateDeviceId($info) {
		$data = array (
				'reg_id' => $info ['reg_id'],
				'fcm_id' => $info ['fcm_id'],
				'user_id' => $info ['user_id'] 
		);
		$this->db->where ( array (
				'device_id' => $info ['device_id'],
				'device_type' => $info ['device_type'] 
		) );
		$res = $this->db->update ( $this->device_table, $data );
		if ($res) {
			return true;
		} else {
			return false;
		}
	}
	
	/*
	 * Insert Device Information
	 * Table: wo_user_devices
	 */
	public function InsertDeviceInformation($info) {
		$data = array (
				'device_id' => $info ['device_id'],
				'user_id' => $info ['user_id'],
				'reg_id' => $info ['reg_id'],
				'fcm_id' => $info ['fcm_id'],
				'device_type' => $info ['device_type'] 
		);
		$res = $this->db->insert ( $this->device_table, $data );
		if ($res) {
			return true;
		} else {
			return false;
		}
	}
	
	/*
	 * Update User
	 * Table: wo_users
	 * Parameters: user_id
	 */
	public function update($userid) {
		try {
			$data = $this->input->post ();
			unset ( $data ['key'] );
			$file = $this->uploadProfilePic ();
			if ($file ['file_name'] != '') {
				$data ['user_pic'] = $file ['file_name'];
			}
			$this->db->set ( 'modification_date', 'NOW()', false );
			$this->db->where ( 'user_id', $userid );
			$update = $this->db->update ( $this->user_table, $data );
			if ($update) {
				$res = $this->getUserInfoById ( $userid );
				$message = array (
						'status' => true,
						'response_code' => '1',
						'message' => "User Updated Successfully",
						'data' => $res 
				);
			} else {
				$message = array (
						'status' => false,
						'response_code' => '0',
						'message' => "Database error occured" 
				);
			}
		} catch ( Exception $ex ) {
			$message = array (
					'status' => false,
					'response_code' => '0',
					'message' => $ex->getMessage () 
			);
		}
		return $message;
	}
	
	/*
	 * To Get the Details of a user By user_id
	 * Table: wo_users
	 */
	public function getUserInfoById($user_id) {
		$out = $this->db->select ( 'user_id,name,user_mobile,user_address,country_code,user_profile_status,if(user_pic="","null" ,CONCAT("' . base_url ( 'uploads/users/profile' ) . '/",user_pic ) ) user_pic' )->where ( 'user_id', $user_id )->get ( $this->user_table )->row ();
		return $out;
	}
	
	/**
	 * ription : To Upload Profile Pic
	 *
	 * @param
	 *        	$id
	 * @return bool @date : 05/10/2016
	 * @method : Upload Profile Pic
	 */
	protected function uploadProfilePic() {
		$config = array ();
		ini_set ( 'upload_max_filesize', '200M' );
		ini_set ( 'post_max_size', '200M' );
		ini_set ( 'max_input_time', 3000 );
		ini_set ( 'max_execution_time', 3000 );
		$config ['upload_path'] = IMAGESPATH . 'users/profile/';
		$config ['allowed_types'] = '*';
		$config ['file_name'] = md5 ( uniqid ( rand (), true ) );
		$this->load->library ( 'upload', $config );
		$this->upload->initialize ( $config );
		if ($this->upload->do_upload ( 'user_pic' )) {
			$info = $this->upload->data ();
			return $info;
		}
	}
	
	/*
	 * To get the friend list of users by phone numbers
	 */
	public function getUserFriendsList($userID) {
		$contacts = json_decode ( $this->input->post ( 'contacts' ) ); // print_r($contacts); die();
		$arr = array ();
		foreach ( $contacts as $k ) {
			
			// print_r($k); die();
			
			if (0 === strpos ( $k, '0' )) {
				$k = $this->str_replace_first ( '0', '', $k );
			}
			
			if (0 === strpos ( $k, '91' )) {
				$k = $this->str_replace_first ( '91', '', $k );
			}
			
			if (0 === strpos ( $k, '+91' )) {
				$k = $this->str_replace_first ( '+91', '', $k );
			}
			
			$k = str_replace ( ' ', '', $k );
			
			array_push ( $arr, $k );
		}
		
		// print_r($arr); die();
		if (! empty ( $arr )) {
			$ownMobile = $this->input->post ( 'user_mobile' );
			$query = $this->db->select ( 'user_id,name,user_mobile,user_address,country_code,user_profile_status,if(user_pic="","null" ,CONCAT("' . base_url ( 'uploads/users/profile' ) . '/",user_pic ) ) user_pic' )->where_in ( 'user_mobile', $arr )->where ( 'user_mobile !=', $ownMobile )->where ( 'is_active', '1' )->from ( $this->user_table )->get ();
			$res = $query->result_array ();
			// echo $this->db->last_query ();
			// die ();
			if (! empty ( $res )) {
				$friendsIds = array_column ( $res, 'user_id' );
				$IDs = array ();
				$i = 0;
				foreach ( $friendsIds as $friendId ) {
					$IDs [$i] ['friend_id'] = $friendId;
					$IDs [$i] ['user_id'] = $userID;
					$i ++;
				}
				$this->deleteUserFriends ( $userID );
				$this->db->insert_batch ( $this->friends_table, $IDs );
			}
			
			return $res;
		} else {
			return false;
		}
	}
	
	/*
	 * First Delete All the friends of User based on the user_id
	 */
	public function deleteUserFriends($userID) {
		$this->db->where ( 'user_id', $userID );
		$this->db->delete ( $this->friends_table );
	}
	public function updateStatus($mobile) {
		$data ['is_active'] = '1';
		$this->db->where ( 'user_mobile', $mobile );
		$update = $this->db->update ( $this->user_table, $data );
		if ($update) {
			return true;
		} 

		else {
			return false;
		}
	}
}

?>