<section>
    <div class="section-body">
        <div class="row">
            <!-- BEGIN ALERT - REVENUE -->
            <div class="col-md-3 col-sm-6">
                <div class="card">
                    <div class="card-body no-padding">
                        <div class="alert alert-callout alert-info no-margin">
							<?php 
							$query = get_field_value('wo_users','user_id',1,1);
							?>
                            <strong class="pull-right text-success text-lg"><?php echo $query->num_rows();?><i class="fa fa-users"></i></strong>
                            <strong class="text-xl">Total Users</strong><br/>
                            <span class="opacity-50">Total Users</span>
                        </div>
                    </div><!--end .card-body -->
                </div><!--end .card -->
            </div><!--end .col -->
            <!-- END ALERT - REVENUE -->


        </div><!--end .row -->
        
    </div><!--end .section-body -->
</section>