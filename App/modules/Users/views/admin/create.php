
<section>
    <div class="contain-lg">
        <!-- BEGIN BASIC VALIDATION -->
        <div class="row">
            <div class="col-md-12">
                <form class="form form-validate floating-label" enctype="multipart/form-data" novalidate="novalidate" method="post">
                    <div class="card-head style-primary">
                        <div class="tools pull-left">
                            <header>Add User</header>
                        </div>
                        <div class="tools">
                            <a class="btn btn-flat hidden-xs" href="<?php echo base_url('admin/user'); ?>"><span class="glyphicon glyphicon-arrow-left"></span> &nbsp;Back</a>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                             <?php if (@$message): ?>
                                <div class="alert alert-callout alert-success" role="alert">
                                    <strong>Success!</strong> <?php echo $message; ?>
                                </div>
                            <?php endif; ?>
                            <?php if (@$error): ?>
                                <div class="alert alert-callout alert-warning" role="alert">
                                    <strong>Warning!</strong> <?php echo $error; ?>
                                </div>
                            <?php endif; ?>
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" autocomplete="off" id="name" value="<?php echo set_value('name'); ?>" name="name" required data-rule-minlength="2">
                                
                            </div>
                            <div class="form-group">
                                <label for="alias">Email</label>
                                <input type="email" class="form-control" autocomplete="off" id="user_email" value="<?php echo set_value('user_email'); ?>" name="user_email" required >
                            </div>
                            <div class="form-group">
                                <label for="alias">Country Code</label>
                                <input type="text" class="form-control" autocomplete="off" id="country_code" value="<?php echo set_value('country_code'); ?>" name="country_code" required>
                            </div>
                            <div class="form-group">
                                <label for="alias">Mobile</label>
                                <input type="text" class="form-control" autocomplete="off" id="user_mobile" value="<?php echo set_value('user_mobile'); ?>" name="user_mobile" required >
                            </div>
                            <div class="form-group">
                                <label for="alias">Address</label>
                                <input type="text" class="form-control" autocomplete="off" id="user_address" value="<?php echo set_value('user_address'); ?>" name="user_address" >
                            </div>
                            
                            <div class="form-group">
                                <label for="alias">Password</label>
                                <input type="password" class="form-control" autocomplete="off" id="user_password" value="<?php echo set_value('user_password'); ?>" name="user_password"   >
                            </div>
                            <div class="form-group">
                                            <div class="form-group">
                                                <input class="form-control" type="file" name="user_pic" id="user_pic">
                                            </div>
                            </div>
                            <div class="form-group">

                                <label for="is_active">Status</label>
                                <select class="form-control" name="is_active" id="is_active" value="<?php echo set_value('is_active'); ?>" required>
                                    <option value="">Select</option>
                                    <option value="1">Active</option>
                                    <option value="0">Inactive</option>
                                </select>
                            </div>

                        </div><!--end .card-body -->
                        <div class="card-actionbar">
                            <div class="card-actionbar-row">
                                <button type="submit" class="btn ink-reaction btn-raised btn-primary btn-loading-state" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Loading...">Create<div style="top: 26px; left: 32.5px;" class="ink"></div></button>
                            </div>
                        </div><!--end .card-actionbar -->
                    </div><!--end .card -->
                </form>
            </div><!--end .col -->
        </div><!--end .row -->
        <!-- END BASIC VALIDATION -->

    </div><!--end .section-body -->
</section>